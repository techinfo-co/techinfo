---
slug: teletravail-methodes
title: "Covid 19 : Nouvelles méthodes de travail en entreprise"
authors: quentin
tags: [informatique, environnement, écologie]
---

La Covid 19 a poussé les entreprises à se mettre au télétravail. Cela a-t-il fonctionné dans vos équipes ?
Comment votre entreprise s'est adaptée à ce nouveau mode de travail ? Des habitudes sont-elles restées ?
Avez-vous essayé de mesurer l'impact environnemental que cela pouvait avoir ?

Nous proposons un échange à ce sujet sur Jitsi le 5 février prochain à 18h :
https://meet.jit.si/ActiveReachesThinkSelfishly