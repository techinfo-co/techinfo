---
slug: migration-docusaurus-2
title: Passage à la version 2 de Docusaurus
authors: quentin
tags: [association]
---

La version 2 de Docusaurus est officiellement lancée. Vous trouverez une présentation des modifications apportées par cette nouvelle version ici :
https://docusaurus.io/blog/2022/08/01/announcing-docusaurus-2.0

Nous avons migré le site vers cette version toute récente.

Nous profitons de ce changement de version pour introduire une nouvelle fonctionnalité : la barre de recherche.
En espérant que cela vous soit utile. N'hésitez pas à nous faire part de vos idées d'amélioration.