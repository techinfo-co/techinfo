---
slug: comment-apprendre-coder
title: Comment apprendre à coder ?
authors: [quentin]
tags: [apprendre, code, programmation]
---
Voici une liste de site Internet qui vous aideront à vous initier aux langages de programmation :
- https://openclassrooms.com/
- https://www.developpez.com/
- https://zestedesavoir.com/
- https://www.codingame.com/
