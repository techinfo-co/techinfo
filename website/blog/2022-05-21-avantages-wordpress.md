---
slug: avantages-inconvenients-wordpress
title: Avantages et inconvénients du CMS WordPress
authors: [quentin]
tags: [cms, wordpress]
---
Retrouvez le replay de notre webinaire consacré au plus célèbre de Content Management System : WordPress.
Un live de plus de 2 heures riches en retour d'expérience grâce à la participation des viewers :
https://www.twitch.tv/videos/150001251