---
slug: series-films-informatique
title: Séries et films autour de l'informatique
authors: quentin
tags: [technologie, informatique, film, serie]
---
Voici quelques séries et films autour de l'informatique.

Inspiré d'une histoire vraie :
- Imitation Game
- 3615 Monique
- Social Network
- Super Pumped, la face cachée d'Uber
- Snowden

On peut aussi évoquer autour des nouvelles technologies :
- Black Mirror
- Devs
- Silicon Valley
- Made For Love
- Mr Robot
- Stalk
- Billion Dollar Code
- How to sell drugs on the internet (fast)
- Person of Interest

N'hésitez pas à nous envoyer vos suggestions.