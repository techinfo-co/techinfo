---
slug: alternatives-open-source
title: Alternatives open-source
authors: quentin
tags: [informatique, open-source]
---

Les alternatives open-source peuvent représenter un intérêt écologique. Contrairement à certains logiciels propriétaires, elles ne sont pas pensées pour pousser au remplacement de son équipement informatique. L'un des intérêts des logiciels à source ouverte, c'est que tout le monde peut participer à son développement et donc le rendre compatible avec toute sorte d'équipement.

Pour vous aider à vous y retrouver, voici quelques suggestions pour remplacer des outils propriétaires par des équivalents open-source.

<!--truncate-->

Suite Microsoft Office -> Libre Office
Discord -> ReVolt
Slack/Teams -> Rocket.Chat
Windows/MacOs -> Ubuntu
Youtube -> Peertube
Chrome -> Chromium (Ungoogled) ou Firefox
Clubhouse -> Jam
Xsplit -> OBS
Adobe Premiere -> Kdenlive
Twitter -> Mastodon
Zoom -> Jitsi
Github -> Gitlab
Google Drive -> Nextcloud
Google Forms -> Framaforms
Doodle -> Rallly

N'hésitez pas à nous dire ce que vous en pensez et à nous envoyer vos suggestions.