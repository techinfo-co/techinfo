---
slug: securite-smartphone
title: La sécurité sur smartphone
authors: quentin
tags: [technologie, informatique, securite]
---

Votre smartphone, vous l’utilisez au quotidien pour aller sur Internet, envoyer des messages. Il concentre inévitablement beaucoup
d'informations (parfois sensibles) vous concernant vous ou l'entreprise pour laquelle vous travaillez.
De plus, il est connecté en permanence à différents types de réseaux.
Il est évident que si quelqu'un de mal intentionné met la main sur ce que contient votre téléphone, il peut vous nuire...

Passez du côté obscur de votre smartphone !

<!--truncate-->

La menace est réelle ! Quelques statistiques :
16 millions de smartphones infectés dans le monde (Alcatel-Lucent)
1,5 millions de SMS indésirables ont été signalés en 2015 (en France sur 33700 SPAM)

##Comment les hackers parviennent-ils à nous atteindre ?

Le Bluetooth présente un nombre important de vulnérabilités qui sont des portes ouvertes pour les hackers. Le NFC (paiement sans contact) gagne en importance et devient aussi une nouvelle piste exploitée par les pirates.

Le wifi de votre smartphone utilise des ondes radio facilement décryptables si leur chiffrement est faible. Quelqu'un de mal intentionné peut donc récupérer les mots de passe et les coordonnées bancaires via votre connexion. Il faut également se méfier des hotspots publics : un hacker connecté au réseau peut enregistrer tous les échanges effectués.

Un pirate peut également accéder à vos données en vous envoyant un MMS piégé qui exécute du code malveillant. Vous pouvez également recevoir des SMS vous faisant croire à de faux messages vocaux. Le numéro indiqué est faux et surtaxé. (SMS Phishing)

Une autre vulnérabilité concerne les applications. Il ne faut pas installer des applications dont la provenance n'est pas vérifiée (fausse publicité). Elles peuvent s'avérer malveillantes et peuvent profiter de privilèges pour accéder aux données de votre téléphone.

##Comment se prémunir face à ces menaces ?

Mettre à jour ses applications et son système d’exploitation (bien que parfois compliqué voire impossible pour des raisons commerciales)

Toujours se méfier des appels, SMS et MMS reçus par des numéros que nous ne connaissons pas

Préférer utiliser des applications vérifiées (sur le Google Play ou l’AppStore) et penser à lire les commentaires laissés par les autres utilisateurs

Utiliser des mots de passe (et code PIN) complexes

Désactiver tout ce qui n’est pas utile (Bluetooth après échange, Wifi quand on est connecté en 3G…)

Eviter de se servir de son téléphone en root, cela facilite le travail des pirates

Chiffrer sa connexion en passant par un VPN lors des communications sensibles

## Comment détecter les malwares et s’en débarrasser ?

L’apparition intempestive de publicités
Des publicités vous envahissent pendant votre navigation dans d’autres applications à priori “normales”.

Une consommation Internet (data) élevée
Certains malwares nécessitent des connexions Internet importantes et régulières, pour télécharger des publicités, des fichiers volumineux, ou envoyer un grand nombre de messages.

Des factures anormalement salées
Certaines applications malveillantes sont conçues pour communiquer discrètement vers des numéros surtaxés (appels, SMS).

Des applications « qui se sont installées toutes seules »
Certains malwares mobiles sont conçus pour télécharger automatiquement des applications payantes sur Google Play, voire sur d’autres plateformes alternatives.

Une demande de rançon pour débloquer l’appareil
Les ransomwares existent aussi sur les smartphones.
Ils chiffrent les données contenues sur l’appareil et exigent le paiement d’une somme pour déchiffrer les données.


## Des solutions pour s’en débarasser
Utiliser un antivirus et un anti adware fiables
Faire des restaurations (à partir de sauvegardes préalables)
Désinstaller toute application semblant suspecte

