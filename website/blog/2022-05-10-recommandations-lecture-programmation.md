---
slug: recommandations-lecture-programmation
title: Recommandations de lecture pour s'améliorer en programmation
authors: [quentin]
tags: [livre, programmation]
---
Voici quelques ouvrages que nous pouvons vous conseiller de lire si vous souhaitez vous améliorer en programmation.

- Software Craft : TDD, Clean Code et autres pratiques essentielles
- Clean Architecture: A Craftsman's Guide to Software Structure and Design
- Domain-Driven Design: Tackling Complexity in the Heart of Software
- Plongée au cœur des PATRONS DE CONCEPTION
- Modern Software Engineering Doing What Works to Build Better Software Faster


Ils vous aideront à découper vos programmes et améliorer la qualité de votre code pour rendre vos projets plus facilement maintenables.