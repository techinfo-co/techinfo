---
slug: reduire-bilan-carbone-informatique
title: Comment réduire son bilan carbone informatique ?
authors: quentin
tags: [informatique, environnement, écologie]
---

Voici quelques conseils pour réduire le bilan carbone (émission de CO2) liées aux activités informatiques en tant qu'utilisateur.

<!--truncate-->

Bon, la première astuce risque de ne pas étonner grand monde : garder son matériel le plus longtemps possible est sans doute ce qu'il y a de plus efficace pour optimiser son bilan carbone.

Au moment d'acheter un nouvel équipement, essayez de voir si vous ne pouvez pas réparer ou upgrader celui que vous avez déjà. Vous pouvez également vous tourner vers le marché de l'occasion avant d'acheter un équipement neuf dont l'impact carbone sera forcément plus élevé.

Ensuite dans l'usage, si vous allez beaucoup sur Internet, il faut évidemment essayer de limiter sa consommation. Pour cela, il faut s'attaquer aux vidéos. Elles représentent la plus grosse part du trafic mondial.

On peut donc conseiller d'éviter les sites qui mettent en oeuvre de mauvaises pratiques telles que le lancement automatique de vidéos en fond. Dans la mesure du possible, essayez de configurer les applications pour éviter qu'elles déclenchent automatiquement ces vidéos non désirées.

Il est également déconseillé d'utiliser le mécanisme de scroll infini. Ce système est généralement gourmand en ressource et pousse à la consommation.