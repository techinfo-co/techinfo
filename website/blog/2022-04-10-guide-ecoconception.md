---
slug: guide-ecoconception
title: Guide Ecoconception
authors: quentin
tags: [informatique, programmation, ecologie]
---

Le gouvernement a mis en place une mission interministérielle spécialisée au sujet du numérique écoresponsable.

Ils ont publié un guide pour indiquer un ensemble de bonnes pratiques dans le cadre du développement d'un service web.

Voici le lien vers le Référentiel général d'écoconception de services numériques (RGESN) :
https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/

Vous trouverez plein d'autres recommandations intéressantes au sujet des nouvelles technologies et de la limitation de leur impact ici :
https://ecoresponsable.numerique.gouv.fr/