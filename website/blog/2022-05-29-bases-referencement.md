---
slug: bases-referencement
title: Les bases sur le référencement
authors: quentin
tags: [referencement, informatique]
---

Aujourd'hui nous vous proposons un article sur les bases du référencement sur Internet. Comment faire connaitre son site auprès des moteurs de recherches et optimiser son contenu ?

<!--truncate-->
Pour optimiser le SEO (référencement naturel), il est possible de travailler selon 4 axes :
le contenu
le netlinking
l’accessibilité
la confiance

## Contenu

### Description
Au niveau du contenu, il faut d’abord choisir des mots-clés, qu’il sera possible de placer dans les balises meta description des différentes pages à référencer. Pour être sûr que l’expression apparaisse, il faut idéalement la placer dans les 100 premiers caractères de la description de la page. Google est susceptible d’afficher cette description dans les résultats. Il est conseillé qu’elle soit unique pour chaque page.

### Arborescence

On peut ensuite travailler sur l’arborescence des URIs et regrouper le contenu dans des groupes logiques. Pour cela, on peut faire appel à l’URI rewriting, qui est régulièrement utilisé par les CMS lorsque les noms des pages sont dynamiques. Google recommande que les URIs soient simples à comprendre. Il faut qu’une seule version d’URI renvoie vers un même document et que tous les caractères soient en minuscules.

Il faut prévoir que l’utilisateur puisse supprimer la fin de l’URI et répondre à cette requête avec un résultat pertinent. Il est conseillé de prévoir des pages 404 “utiles”.

Google recommande aussi l’utilisation d’un fil d’Ariane, qui met en évidence le parcours de l’utilisateur.

### Contenu et structure
On peut optimiser l’utilisation du mot-clé ou de l’expression clé dans le contenu de la page : en la plaçant dans la balise title ainsi que l’unique balise h1 de la page. Les titres doivent être uniques, descriptifs et courts. Pour éviter de déclencher un signal de suroptimisation, il est conseillé d’avoir un titre de page proche mais différent de la balise h1. Il est recommandé de répéter 3 à 4 fois maximum le mot-clé visé dans un total de 300 mots présents dans la page.
Il est également conseillé de décrire le contenu des images avec la balise alt. Dans les bonnes pratiques, il est aussi recommandé de publier fréquemment de nouveaux contenus. Le moteur de recherche s’en servirait comme “indicateur d’activité”.

On peut structurer la page avec les balises sémantiques html5 et la découper en sections logiques. 

### Liens internes
Les liens internes sont également importants : si le robot est amené à rencontrer souvent un même lien, il sera amené à le hiérarchiser en conséquence (en fonction de son parcours sur le site).

Pour “faciliter” l’exploration du site par les robots des moteurs de recherches, il est possible de créer un sitemap. Ce sitemap contient une liste d’URI pointant vers les contenus à référencer.
Netlinking
Il est conseillé d’avoir des liens qui pointent vers le site à référencer depuis plusieurs autres sites, de différents types (blogs, forums, sites institutionnels).
Les moteurs de recherches établissent des indices de confiance sur les différents sites rencontrés. Dans l’idéal, il faudrait avoir un lien sur un site dont la thématique est proche et la confiance est bonne. Pour optimiser ce lien, il doit être placé dans un contenu dont les liens voisins abordent des sujets liés.

L’expression qui accompagne ce lien doit varier d’un site à l’autre. Il est précisé qu’il est efficace d’obtenir des liens profonds (qui pointe directement vers une page précise du site).

Ce qu’il faut éviter, c’est d’avoir un même lien qui pointe vers notre site dans le footer de toutes les pages d’un autre site.

## Accessibilité
Le contenu doit être accessible par tous devices du marché. Google indique que le responsive design fait parti des critères sur lesquels il se base pour afficher les résultats sur mobile. La structure de la page est également prise en compte (validation W3C).

## Performance
La rapidité d’affichage des pages est aussi importante, ce qui peut poser problème par rapport à un site entièrement fait en javascript car le robot doit d’abord charger le script. Google alloue un temps spécifique (crawl budget) pour parcourir le site, ce qui peut être pénalisant si les pages mettent du temps à se construire. Mais Google parvient à afficher les pages “aussi bien que les navigateurs actuels” (https://webmasters.googleblog.com/2015/10/deprecating-our-ajax-crawling-scheme.html), ce qui ne devrait pas poser de problème avec les sites en ajax. Il est également indiqué que Bing supporte seulement le prerender, qui consiste à renvoyer sous forme de fragments, le contenu de la page directement au format HTML. Avec cette solution, il y a cependant un risque de cloaking : Google peut détecter cela comme une façon différente de présenter la page. Après l’avoir conseillé dans sa documentation, Google a indiqué que cette façon de procéder était “dépréciée”.

## Confiance

L’autorité du site va dépendre des liens entrants rencontrés sur le web par le moteur de recherches (quantité et qualité, voir la partie netlinking).
L’historique est pris en compte (changement de propriétaire, de thématique, piratage…).


Il ne faut pas négliger les réseaux sociaux qui permettent de générer des liens naturels. Il faut aussi rappeler que le moteur de recherche se base sur la géolocalisation de l’utilisateur, son historique de recherche et la durée de sa visite sur les sites pour déterminer l’ordre d’affichage des résultats.