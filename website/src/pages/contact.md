---
title: Nous contacter
---

# Contacter l'association

Vous pouvez nous contacter pour signaler une erreur, nous aider à améliorer notre contenu ou nous envoyer des suggestions de sujets.
Vous pouvez également participer à l'un de nos prochains échanges.

Pour cela, il suffit de remplir ce formulaire :
https://framaforms.org/users/techinfo/contact