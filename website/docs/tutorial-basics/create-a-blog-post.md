---
sidebar_position: 3
---

# Logistique

La logistique et le transport sont également des secteurs d'innovation. Nous aurons l'occasion d'y revenir de façon plus détaillée en évoquant sa connexion avec le système d'informations.

:::caution En développement

Cette partie arrivera plus tard sur notre site.

:::