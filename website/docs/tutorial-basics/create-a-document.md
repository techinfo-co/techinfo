---
sidebar_position: 2
---

# Programmation

Dans cette catégorie, nous allons partager un ensemble de bonnes pratiques. On propose régulièrement des échanges pour rester en veille sur les différentes technologies qui émergent au quotidien.
Ces vastes sujets s'accompagnent également de la question de l'impact environnemental qui sera abordé.