---
sidebar_position: 1
---

# Informatique

Dans les articles du thème Informatique, vous retrouverez des conseils pour entrenir vos équipements. Nous vous partagerons également des recommandations de logiciels et des astuces pour optimiser votre ordinateur.

La question de la cyber-sécurité sera également abordée.

:::info Au coeur des technologies

L'informatique est utilisée partout. Tous les services sont en train de se digitaliser. Ce thème nous parait donc primordial.

:::