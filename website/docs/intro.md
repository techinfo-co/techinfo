---
sidebar_position: 1
---

# TechInfo & Co

Bienvenue sur le site de l'association **TechInfo & Co**.

## Que puis-je trouver sur ce site ?

Sur ce site, vous allez découvrir des articles et analyses sur les nouvelles technologies. Passionnés par ce domaine, c'est également dans un but pédagogique que nous avons créé ce site.
Nous allons essayons de rendre concrets des concepts qui peuvent être abstraits.

## Qui sont les auteurs des contenus publiés ?
Les auteurs sont des professionnels en activité. Ils sont généralement spécialisés dans la thématique dans laquelle ils écrivent.